import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';


import { AppComponent } from './app.component';
import { SinginComponent } from './singin/singin.component';
import { SignupComponent } from './signup/signup.component';
import { AppRoutingModule } from './/app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AccountService } from './signup/account.service';
import { SigninService } from './singin/signin.service';
import { UserComponent } from './user/user.component';
import { HeaderComponent } from './header/header.component';
import { MyorderComponent } from './myorder/myorder.component';
import { OrderService } from './order.service';
import { OrdercreateComponent } from './ordercreate/ordercreate.component';
import { AdminComponent } from './admin/admin.component';
import { BankheaderComponent } from './bankheader/bankheader.component';
import { OrderComponent } from './order/order.component';
import { OrderDetailComponent } from './order-detail/order-detail.component';
import {AuthGuard} from './auth.guard';


@NgModule({
  declarations: [
    AppComponent,
    SinginComponent,
    SignupComponent,
    UserComponent,
    HeaderComponent,
    MyorderComponent,
    OrdercreateComponent,
    AdminComponent,
    BankheaderComponent,
    OrderComponent,
    OrderDetailComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    TableModule,
    DropdownModule,
    BrowserAnimationsModule
  ],
  providers: [AccountService, SigninService, OrderService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
