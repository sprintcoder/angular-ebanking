import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {SigninService} from './singin/signin.service';

@Injectable()
export class OrderService {
  private orderUrl = 'http://localhost:8080/orders';

  constructor(private http: HttpClient, private authService: SigninService) { }

  getMyOrders(page: number, status: string = null): Observable<any> {
    let httpParams;

    if (status) {
      httpParams = new HttpParams().set('page', '' + page).append('status', status);
    } else {
      httpParams = new HttpParams().set('page', '' + page);
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.authService.token }),
      params: httpParams
    };
    return this.http.get(this.orderUrl + '/myorders', httpOptions);
  }

  getOrders(page: number, status: string = null): Observable<any> {
    let httpParams;

    if (status) {
      httpParams = new HttpParams().set('page', '' + page).append('status', status);
    } else {
      httpParams = new HttpParams().set('page', '' + page);
    }
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.authService.token }),
      params: httpParams
    };
    return this.http.get(this.orderUrl, httpOptions);
  }

  getOrder(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.authService.token })
    };
    return this.http.get(this.orderUrl + '/' + id, httpOptions);
  }

  createOrder (order): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.authService.token })
    };
    return this.http.post(this.orderUrl, order, httpOptions);
  }


  updateOrder (order): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.authService.token })
    };
    return this.http.patch(this.orderUrl + '/' + order.id, order, httpOptions);
  }

}
