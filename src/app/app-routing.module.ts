import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SignupComponent} from './signup/signup.component';
import {SinginComponent} from './singin/singin.component';
import {UserComponent} from './user/user.component';
import {MyorderComponent} from './myorder/myorder.component';
import {OrdercreateComponent} from './ordercreate/ordercreate.component';
import {AdminComponent} from './admin/admin.component';
import {OrderComponent} from './order/order.component';
import {OrderDetailComponent} from './order-detail/order-detail.component';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/signin', pathMatch: 'full' },
  { path: 'signin', component: SinginComponent },
  {path: 'signup', component: SignupComponent},
  {path: 'user', component: UserComponent, children: [
      {path: 'myorders', component: MyorderComponent},
      {path: 'create', component: OrdercreateComponent}
    ], canActivate: [AuthGuard]
  },
  {path: 'admin', component: AdminComponent, children: [
      {path: 'orders', component: OrderComponent},
      {path: 'detail/:id', component: OrderDetailComponent}
    ], canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
