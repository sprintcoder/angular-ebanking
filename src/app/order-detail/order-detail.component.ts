import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {OrderService} from '../order.service';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.component.html',
  styleUrls: ['./order-detail.component.css']
})
export class OrderDetailComponent implements OnInit {
  order: any;
  errors: any[];
  isSaved: boolean;

  constructor(private route: ActivatedRoute, private orderService: OrderService) { }

  ngOnInit() {
     this.order = {};
     this.errors = [];
     const id = +this.route.snapshot.paramMap.get('id');
     this.orderService.getOrder(id).subscribe(
       (res) => this.order = res,
       (err) => console.log(err)
     );
  }

  onUpdate(status: string) {
    const orderPatch = {id: this.order.id, status: status};
    this.orderService.updateOrder(orderPatch).subscribe(
      (res) => {
        this.order.status = res.status;
        this.isSaved = true;
      },
      (err) => {
        if (err.status === 422) {
          console.log(err);
          this.errors = err.error.fieldErrors;
          this.isSaved = false;
        }
      }
    );
  }

}
