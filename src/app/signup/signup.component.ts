import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {AccountService} from './account.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  accountForm: FormGroup;
  isSaved: boolean;
  errors: any[];

  constructor(private fb: FormBuilder, private accountService: AccountService) { }

  ngOnInit() {
    this.accountForm = this.fb.group({
      username: [''],
      password: [''],
      accountNumber: ['']
    });
    this.errors = [];
  }

  onSubmit() {
    if (this.accountForm.valid) {
      const account = {
        username: this.accountForm.value.username,
        password: this.accountForm.value.password,
        accountNumber: this.accountForm.value.accountNumber
      };
      this.accountService.addAccount(account)
        .subscribe(
          () => {
            this.isSaved = true;
            this.errors = [];
          },
          error => {
            this.isSaved = false;
            if (error.status === 422) {
               console.log(error);
               this.errors = error.error.fieldErrors;
            }
          }
        );
    }
  }

}
