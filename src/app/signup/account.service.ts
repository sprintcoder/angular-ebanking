import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AccountService {

  private accountUrl = 'http://localhost:8080/accounts';

  constructor(private http: HttpClient) { }

  addAccount (account): Observable<any> {
    return this.http.post(this.accountUrl, account, httpOptions);
  }

}
