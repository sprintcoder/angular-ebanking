import { Component, OnInit } from '@angular/core';
import {LazyLoadEvent, SelectItem} from 'primeng/api';
import {OrderService} from '../order.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  orders: any[];
  cols: any[];
  totalRecords: number;
  loading: boolean;
  statuses: SelectItem[];

  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.loading = true;
    this.statuses = [
      { label: 'All Statuses', value: null },
      { label: 'Set', value: 'SET' },
      { label: 'Executed', value: 'EXECUTED' },
      { label: 'Rejected', value: 'REJECTED' }
    ];
    this.cols = [
      { field: 'id', header: 'Id' },
      { field: 'accountFrom', header: 'Account From' },
      { field: 'accountTo', header: 'Account To' },
      { field: 'amount', header: 'Amount' },
      { field: 'status', header: 'Status' }
    ];
  }

  loadOrdersLazy(event: LazyLoadEvent) {
    this.loading = true;
    let currentPage: number;
    currentPage = event.first / event.rows;
    const status: string = event.filters.status ? event.filters.status.value : null;
    this.orderService.getOrders(currentPage, status)
      .subscribe((res) => {
        this.orders = res._embedded.orderDtoList;
        this.totalRecords = res.page.totalElements;
        this.loading = false;
      });
  }

}
