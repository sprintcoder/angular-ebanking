import { Component, OnInit } from '@angular/core';
import {SigninService} from '../singin/signin.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private authService: SigninService, private router: Router) { }

  ngOnInit() {}


  onSignOut() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

}
