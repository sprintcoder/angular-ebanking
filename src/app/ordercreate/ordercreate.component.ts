import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {OrderService} from '../order.service';

@Component({
  selector: 'app-ordercreate',
  templateUrl: './ordercreate.component.html',
  styleUrls: ['./ordercreate.component.css']
})
export class OrdercreateComponent implements OnInit {

  orderForm: FormGroup;
  isSaved: boolean;
  errors: any[];
  globalErrors: any[];

  constructor(private fb: FormBuilder, private orderService: OrderService) { }

  ngOnInit() {
    this.orderForm = this.fb.group({
      accountTo: [''],
      amount: ['']
    });
    this.errors = [];
    this.globalErrors = [];
  }

  onSubmit() {
    if (this.orderForm.valid) {
      const order = {
        accountTo: this.orderForm.value.accountTo,
        amount: this.orderForm.value.amount,
      };
      this.orderService.createOrder(order)
        .subscribe(
          () => {
            this.isSaved = true;
            this.errors = [];
          },
          error => {
            this.isSaved = false;
            if (error.status === 422) {
              console.log(error);
              this.errors = error.error.fieldErrors;
              this.globalErrors = error.error.globalErrors;
            }
          }
        );
    }
  }

}
