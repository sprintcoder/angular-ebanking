import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

@Injectable()
export class SigninService {
  public token: string;
  private tokenUrl = 'http://localhost:8080/oauth/token';

  constructor(private http: HttpClient, private router: Router) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.token = currentUser && currentUser.token;
  }

  login(username: string, password: string) {

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
        'Authorization': 'Basic ' + btoa('angular-ebank:secret')})
    };
    const body = new URLSearchParams();
    body.set('grant_type', 'password');
    body.set('client_id', 'angular-ebank');
    body.set('username', username);
    body.set('password', password);
    this.http.post(this.tokenUrl, body.toString(), httpOptions)
      .subscribe(
        (res: any) => {
          this.token = res.access_token;
          const authority = res.authorities[0].authority;
          localStorage.setItem('currentUser',
            JSON.stringify({ username: username, token: this.token, authority: authority }));
          console.log(authority);
          if (authority === 'USER') {
            this.router.navigate(['/user']);
          } else if (authority === 'ROLE_ADMIN') {
            this.router.navigate(['/admin']);
          }
        },
        (err) => console.log(err)
      );
  }

  logout() {
    this.token = null;
    localStorage.removeItem('currentUser');
  }

}
