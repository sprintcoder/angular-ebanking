import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {SigninService} from './signin.service';

@Component({
  selector: 'app-singin',
  templateUrl: './singin.component.html',
  styleUrls: ['./singin.component.css']
})
export class SinginComponent implements OnInit {

  constructor(private signinService: SigninService) { }

  ngOnInit() {
  }

  onSignin(form: NgForm) {
    const username = form.value.username;
    const password = form.value.password;
    this.signinService.login(username, password);
  }

}


