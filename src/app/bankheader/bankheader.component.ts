import { Component, OnInit } from '@angular/core';
import {SigninService} from '../singin/signin.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-bankheader',
  templateUrl: './bankheader.component.html',
  styleUrls: ['./bankheader.component.css']
})
export class BankheaderComponent implements OnInit {

  constructor(private authService: SigninService, private router: Router) { }

  ngOnInit() {
  }

  onSignOut() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

}
