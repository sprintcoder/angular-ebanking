import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankheaderComponent } from './bankheader.component';

describe('BankheaderComponent', () => {
  let component: BankheaderComponent;
  let fixture: ComponentFixture<BankheaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankheaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankheaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
